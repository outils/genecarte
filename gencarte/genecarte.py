# -*- coding: utf-8 -*-

import re
import copy
import sys
from pyLot.core import Path, ENCODING
from libcsv import read_genecarte_csv_file


# Utilisation du module inkex avec des effets predefinis
import inkex
import simpletransform
import coloreffect

# Le module simplestyle fournit des fonctions pour le parsing des styles
from simplestyle import *
from simpletransform import parseTransform

class EmptyGenerator(object):
    def __init__(self, **kargs):
        self.kargs = kargs
        self.margins = ['0.7cm', '0.7cm', '0.7cm', '0.7cm']

class CsvGenerator(object):

    def __init__(self, *args, **kargs):
        self.kargs = kargs
        self.margins = [0, 0, 0, 0]
        self.csv_path = kargs.get('csv', None)

        self.fields, self.dtypes, self.cards = read_genecarte_csv_file(self.csv_path)
        self.ncopy = len(self.cards)

    def setText(self, text, node):
        istext = (node.tag == '{http://www.w3.org/2000/svg}flowPara' or node.tag == '{http://www.w3.org/2000/svg}flowDiv' or node.tag == '{http://www.w3.org/2000/svg}text')
        if node.get('{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}role') == 'line':
            self.newline = True
        elif istext:
            self.newline = True
            self.newpar = True

        if node.text != None:
            node.text = text
            self.newline = False
            self.newpar = False

        for child in node:
            self.setText(text, child)

        if node.tail != None:
            node.tail = ''

    def setAttrib(self, node, node_id, attrib, value):
        if 'id' in node.attrib and node.attrib['id'] == node_id :
            node.attrib[attrib] = value
        else :
            for sub in node :
                self.setAttrib(sub, node_id, attrib, value)


    def changeText(self, node, identifier, text):
        for sub in node :
            if 'id' in sub.attrib and sub.attrib['id'] == identifier :
              self.setText(text, sub)

    def changeCard(self, effect, node, idx, **kwargs):
        node_dx = kwargs['dx']
        node_dy = kwargs['dy']

        inkex.debug('Edit card %d/%d:' %(idx+1, len(self.cards)))

        for i in range(len(self.fields)):
            identifier = self.fields[i]
            dtype = self.dtypes[i]
            value =self.cards[idx][i]
            inkex.debug('  - %s: %r' %(identifier, value))

            if dtype.lower() == 'text':
                self.changeText(node, identifier, value)
            elif dtype.lower() == 'object':
                image = effect.getElementById(identifier)
        
                if image is None:
                    return
        
                xmin, xmax, ymin, ymax = simpletransform.computeBBox(node)
                img_xmin, img_xmax, img_ymin, img_ymax = simpletransform.computeBBox(image)
        
                # Place it on top/left corner
                dx = -img_xmin
                dy = -img_ymin
        
                # Place it on top/left corner of current node
                dx += xmin
                dy += ymin
        
                # Now place it relatively to node
                posx = (xmax - xmin) / 2. - (img_xmax - img_xmin) / 2.
                posy = 125
                dx += posx
                dy += posy
        
        
                obj = copy.copy(image)
                obj.attrib['transform'] = 'translate(%s,%s)' % (dx, dy)
                node.append(obj)

            elif dtype.lower() == 'color':
                self.changeColor(node, '#ff00ff', value)

            elif dtype.lower() == 'image':
                inkex.debug('image type is not supported yet')


    def changeColor(self, node, old_color, new_color):

        if node.attrib.has_key('style'):
            # see coloreffect.ColorEffect
            style = node.get('style') # fixme: this will break for presentation attributes!
            if style:
                #inkex.debug('old style:'+style)
                declarations = style.split(';')
                for i, decl in enumerate(declarations):
                    parts = decl.split(':', 2)
                    if len(parts) == 2:
                        (prop, val) = parts
                        prop = prop.strip().lower()
                        if prop in coloreffect.color_props:
                            val = val.strip()
                            if val == old_color:
                                declarations[i] = prop + ':' + new_color
                #inkex.debug('new style:'+';'.join(declarations))
                node.set('style', ';'.join(declarations))

        for child in node:
            self.changeColor(child, old_color, new_color)

#a dictionary of unit to user unit conversion factors
uuconv = {'in':90.0, 'pt':1.25, 'px':1, 'mm':3.5433070866, 'cm':35.433070866, 'm':3543.3070866,
          'km':3543307.0866, 'pc':15.0, 'yd':3240 , 'ft':1080}

class GenCartes(inkex.Effect):
    """
    Exemple Inkscape 
    Cree un nouveau calque et dessine des elements de base
    """
    def __init__(self):
        # Appel du constructeur.
        inkex.Effect.__init__(self)

        self.OptionParser.add_option('--script', action='store',
                                                                 type='string',
                                                                 dest='script', default='script.py',
                                                                 help='Script à exécuter')
        self.OptionParser.add_option('--csv', action='store',
                                                                 type='string',
                                                                 dest='csv', default='data.csv',
                                                                 help='CSV (fichier texte brut avec colone séparées par des ";")')
        self.OptionParser.add_option('--mode_pos', action='store',
                                                                 type='int',
                                                                 dest='mode_pos', default=1,
                                                                 help='Card placement')


    def unittouu(self, string):
        '''Returns userunits given a string representation of units in another system'''
        unit = re.compile('(%s)$' % '|'.join(uuconv.keys()))
        param = re.compile(r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
    
        p = param.match(string)
        u = unit.search(string)    
        if p:
            retval = float(p.string[p.start():p.end()])
        else:
            retval = 0.0
        if u:
            try:
                return retval * uuconv[u.string[u.start():u.end()]]
            except KeyError:
                pass
        return retval
    
    def uutounit(self, val, unit):
        return val/uuconv[unit]


    def px(self, data):
        """
        This method works only if called in effect method
        """
        if isinstance(data, (list, tuple, set)):
            return type(data)([self.px(item) for item in data])
        elif isinstance(data, basestring):
            return self.unittouu(data)
        else:
            return data

    def _numberOfCards(self, largeur, hauteur, width='21cm', height='29.7cm',
                       margins = ['0.7cm', '0.7cm', '0.7cm', '0.7cm'], mode_pos = 1):
        """
        margins : left, right, top, bottom
        """
        margins = self.px(margins)
        width = self.px(width)
        height = self.px(height)
        largeur = self.px(largeur)
        hauteur = self.px(hauteur)

        if mode_pos == 1 : # Centered :
            dx = max(margins[0], margins[1])
            dy = max(margins[2], margins[3])
            w = width - 2. * dx
            h = height - 2. * dy
        else :
            dx = margins[0]
            dy = margins[2]
            w = width - dx - margins[1]
            h = height - dy - margins[3]
        nx = int(w / largeur)
        ny = int(h / hauteur)
        if mode_pos == 1 :
            dx = (width - largeur * nx) / 2.
            dy = (height - hauteur * ny) / 2.
        return nx, ny, dx, dy

    def effect(self):

        log = ''
        script = Path(self.options.script.decode(ENCODING)).abspath()
        csv = Path(self.options.csv.decode(ENCODING)).abspath()

        mode_pos = int(self.options.mode_pos)
        self.script = None
        self.csv = None        
        
        if script.isfile():
            self.script = script

        if csv.isfile():
            self.csv = csv
        
        if self.csv is None and self.script is None:
            inkex.debug("%s doesn't exists" % script.shellpath())
            inkex.debug("Only duplicate cards !")

        inkex.debug('%s, %s' % (script, self.script))

        svg = self.document.getroot()


        # Recuperation de la hauteur et de la largeur de la feuille
        width = self.px(svg.get('width'))
        height = self.px(svg.attrib['height'])

        self.w = width
        self.h = height

        log += 'Document:\n'
        log += '  dim (uu)  : %f x %f\n' % (self.w, self.h)
        log += '  dim (unit): %f x %f\n' % (width, height)

        modele = self.selected['model']
        xmin, xmax, ymin, ymax = simpletransform.computeBBox(modele)
        l = (xmax - xmin)
        h = (ymax - ymin)

        log += 'Bounding box:\n'
        log += '  xmin, xmax, ymin, ymax : %f, %f, %f, %f\n' % (xmin, xmax, ymin, ymax)
        log += '  width, height : %f x %f\n' % (l, h)


        if self.script :
            sys.path.append(unicode(self.script.parent))
            module = __import__(self.script.namebase)
            uclass = getattr(module, 'GeneCarte')
            self.uc = uclass()
        elif self.csv:
            self.uc = CsvGenerator(csv=self.csv)
        else :
            self.uc = EmptyGenerator()

        nx, ny, x0, y0 = self._numberOfCards(l, h, self.w, self.h, self.uc.margins, mode_pos)
        if self.script is None and self.csv is None:
            self.uc.ncopy = nx * ny

        page = 0

        for i in range(self.uc.ncopy):

            if i % (nx * ny) == 0 :
                # Creation d'un nouveau calque
                page += 1
                self.layer = inkex.etree.SubElement(svg, 'g')
                self.layer.set(inkex.addNS('label', 'inkscape'), 'Page_%d' % page)
                self.layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
                y = y0

            if (i % nx) == 0 :
                y += h

            x = (i % nx) * l + x0

            dx = -xmin + x
            dy = -ymin + y - h
            xml_carte = copy.copy(modele)
            xml_carte.attrib['id'] = 'card_%d' % i
            xml_carte.attrib['transform'] = 'translate(%s,%s)' % (dx, dy)
            self.uc.changeCard(self, xml_carte, i, dx=dx, dy=dy)
            self.layer.append(xml_carte)

        print >> sys.__stdout__, log

# Execute la fonction "effect" de la classe "GenCartes"
hello = GenCartes()
hello.affect()
