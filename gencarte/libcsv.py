#!/usr/bin/python
# -*- coding: utf-8 -*-

__date__ = u"19 mars 2010"
__author__ = u"INRIA pyLot"

__all__ = [
           u'utf_8_encoder',
           u'unicode_csv_reader',
           u'UTF8Recoder',
           u'UnicodeReader',
           u'UnicodeWriter',
           u'read_genecarte_csv_file',
           ]

import csv
import codecs
import cStringIO

def utf_8_encoder(unicode_csv_data):
  for line in unicode_csv_data:
    yield line.encode(u'utf-8')

def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
  # csv.py doesn't do Unicode; encode temporarily as UTF-8:
  csv_reader = csv.reader(utf_8_encoder(unicode_csv_data),
                          dialect=dialect, **kwargs)
  for row in csv_reader:
    # decode UTF-8 back to Unicode, cell by cell:
    yield [unicode(cell, u'utf-8') for cell in row]

class UTF8Recoder:
  u"""
  Iterator that reads an encoded stream and reencodes the input to UTF-8
  """
  def __init__(self, f, encoding):
    self.reader = codecs.getreader(encoding)(f)

  def __iter__(self):
    return self

  def next(self):
    return self.reader.next().encode(u"utf-8")

class UnicodeReader:
  u"""
  A CSV reader which will iterate over lines in the CSV file "f",
  which is encoded in the given encoding.
  """

  def __init__(self, f, dialect=csv.excel, encoding=u"utf-8", **kwds):
    f = UTF8Recoder(f, encoding)
    self.reader = csv.reader(f, dialect=dialect, **kwds)

  def next(self):
    row = self.reader.next()
    return [unicode(s, u"utf-8") for s in row]

  def __iter__(self):
    return self

class UnicodeWriter:
  u"""
  A CSV writer which will write rows to CSV file "f",
  which is encoded in the given encoding.
  """

  def __init__(self, f, dialect=csv.excel, encoding=u"utf-8", **kwds):
    # Redirect output to a queue
    self.queue = cStringIO.StringIO()
    self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
    self.stream = f
    self.encoder = codecs.getincrementalencoder(encoding)()

  def writerow(self, row):
    lst = []
    for s in row :
      if s :
        lst.append(s.encode(u"utf-8"))
      else :
        lst.append(u'')
    self.writer.writerow(lst)
    # Fetch UTF-8 output from the queue ...
    data = self.queue.getvalue()
    data = data.decode(u"utf-8")
    # ... and reencode it into the target encoding
    data = self.encoder.encode(data)
    # write to the target stream
    self.stream.write(data)
    # empty queue
    self.queue.truncate(0)

  def writerows(self, rows):
    for row in rows:
      self.writerow(row)


class DialectGeneCarte(csv.Dialect):
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    skipinitialspace = True
    lineterminator = '\n'
    quoting = csv.QUOTE_MINIMAL

def read_genecarte_csv_file(path):
    from libcsv import unicode_csv_reader
    f = codecs.open(path, 'r', 'utf-8')
    src = f.readlines()
    f.close()
    reader = unicode_csv_reader(src, dialect=DialectGeneCarte())

    lines = list(reader)
    fields = lines[0]
    dtypes = lines[1]
    cards = lines[2:]

    return fields, dtypes, cards


if __name__ == '__main__':
  import doctest
  doctest.testmod() 
