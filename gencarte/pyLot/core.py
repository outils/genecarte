# -*- coding: utf-8 -*-

#This file is part of pyLot library.
#
# pyLot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyLot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with pyLot.  If not, see <http://www.gnu.org/licenses/>.

__author__ = u'Pierre Puiseux, Guillaume Baty'
__copyright__ = u"Copyright 2011-2012 (C) andheo, Université de Pau et des Pays de l'Adour"
__credits__ = [u'Pierre Puiseux', u'Guillaume Baty']
__license__ = "GNU Lesser General Public License"

u""" path.py - An object representing a path to a file or directory.

Example:

.. code-block:: python

  from pyLot.core import Path
  d = Path('/home/guido/bin')
  for f in d.files('*.py'):
      f.chmod(0755)

This module requires Python 2.2 or later.


  * URL:     http://www.jorendorff.com/articles/python/path
  * Author:  Jason Orendorff <jason.@.jorendorff.com> (and others - see the url!)
  * Date:    7 Mar 2004

  * Adapted for stdlib by: Reinhold Birkenfeld, July 2005
  * Modified by Björn Lindqvist <bjourne.@.gmail.com>, January 2006
  * Modified by Guillaume Baty <guillaume.baty.@.wanadoo.fr>, 2008-2011

"""

__all__ = [
    u'DirPath',
    u'FilePath',
    u'Path',
    u'format_path',
    u'ENCODING',
    u'LANG',
    u'LEVELS',
    u'cpu_count',
    u'error',
    u'uprint',
    u'UserConfigDir'
]

import os
import re
import sys
import locale
import codecs
import datetime

try:
    from hashlib import md5
except ImportError:
    pass

LANG, ENCODING = locale.getdefaultlocale()
ENCODING = locale.getpreferredencoding()

if isinstance(ENCODING, str):
    ENCODING = ENCODING.decode(ENCODING)
else:
    ENCODING = locale.getdefaultlocale()[1]
    if isinstance(ENCODING, str):
        ENCODING = ENCODING.decode(ENCODING)
    else:
        ENCODING = u'UTF-8'

if isinstance(LANG, str):
    LANG = LANG.decode(ENCODING)
else:
    LANG = u'en'

LEVELS = [u'user', u'devel', u'expert']

def tr_(s, *args):
    return s

def error(unicode_str):
    return unicode_str.encode(ENCODING, u'replace')

def uprint(*args, **kargs):
    u""" like python 3 print methods.
    all types are supported.
    unicode is encoded before being printed.
    """
    try:
        stream = kargs['file']
    except KeyError:
        stream = sys.stdout

    try:
        end = kargs['end']
    except KeyError:
        end = ''

    s = ''
    for arg in args:
        if isinstance(arg, unicode):
            s += arg.encode(ENCODING, 'replace')
        else:
            s += arg
    if end:
        s += end

    stream.write(s)


def cpu_count():
    u"""
    Returns the number of CPUs in the system
    Imported from python2.6 multiprocessing module
    """
    # TODO: report bug os.sysconf doesn't support unicode
    if sys.platform == u'win32':
        try:
            num = int(os.environ[u'NUMBER_OF_PROCESSORS'])
        except (ValueError, KeyError):
            num = 0
    elif u'bsd' in sys.platform or sys.platform == u'darwin':
        try:
            num = int(os.popen('sysctl -n hw.ncpu').read())
        except ValueError:
            num = 0
        except IOError:
            num = 1
    else:
        try:
            num = os.sysconf('SC_NPROCESSORS_ONLN')
        except (ValueError, OSError, AttributeError):
            num = 0
    if num >= 1:
        return num
    else:
        raise NotImplementedError(error(u'cannot determine number of cpus'))


# TODO
#   - Better error message in listdir() when self isn't a
#     directory. (On Windows, the error message really sucks.)
#   - Make sure everything has a good docstring.
#   - Add methods for regex find and replace.
#   - Perhaps support arguments to touch().
#   - Could add split() and join() methods that generate warnings.
#   - Note:  __add__() technically has a bug, I think, where
#     it doesn't play nice with other types that implement
#     __radd__().  Test this.

import fnmatch
import glob
import shutil

__version__ = u'2.0.4'

# Universal newline support
_textmode = u'r'
if hasattr(file, u'newlines'):
    _textmode = u'U'

class Path(unicode):
    u"""
    Represents a filesystem path.

    For documentation on individual methods, consult their
    counterparts in os.path.
    """
    special_chars = u'"!'
    # --- Special Python methods.
    def __new__(cls, *args):
        u"""
        Creates a new path object concatenating the *args.  *args
        may only contain Path objects or strings.  If *args is
        empty, an exception is raised.
        """
        if not args:
            raise ValueError, u'At least one argument required'
        else:
            pass
        for arg in args:
            if not isinstance(arg, unicode):
                raise TypeError, tr_(u'E006 - Path, unicode expected, got %(TYPE)s' % dict(
                    TYPE=type(arg).__name__))
        if len(args) == 1:
            if not args[0].strip():
                raise ValueError, tr_(u'E007 - Dangerous value, you must use u"." for current directory')
            return unicode.__new__(cls, *args)
        return cls(os.path.join(*args))

    def __repr__(self):
        return u'%s(%r)' % (self.__class__.__name__, unicode(self))

    def __str__(self):
        return self.encode(ENCODING)

    # Adding a path and a string yields a path.
    def __add__(self, more):
        return self.__class__(unicode.__add__(unicode(self), unicode(more)))

    def __radd__(self, other):
        return self.__class__(other + unicode(self))

    # The / operator joins paths.
    def __div__(self, rel):
        """ fp.__div__(rel) == fp / rel == fp.joinpath(rel)

        Join two path components, adding a separator character if
        needed.
        """
        return self.__class__(os.path.join(self, rel))

    # Make the / operator work even when true division is enabled.
    __truediv__ = __div__

    @classmethod
    def cwd(cls):
        u""" Returns the current working directory as a path object. """
        return Path(os.getcwdu())

    # --- Operations on path strings.
    def join(self, seq):
        raise NotImplementedError, u"To join path : Path(p1, p2, p3, ...)"

    def abspath(self):
        # see: http://bugs.python.org/issue3426
        path = os.path.abspath(self.encode(ENCODING)).decode(ENCODING)
        return self.__class__(path)

    def shellpath(self):
        u"""
        Returns a path directly usable in a shell.
        /home/guillaume/mon "dossier" tordu
        -> "/home/guillaume/mon\ \"dossier\"\ tordu

        As shell is in general used via modules like subprocess, system, etc
        a str is more convenient than unicode so ...

        .. warning ::

          This function returns a :obj`str`, not an :obj`unicode`
          Else, please use :meth:`Path.ushellpath`

        """
        return self.ushellpath().encode(ENCODING)

    def ushellpath(self):
        u"""
        Returns a path directly usable in a shell.
        /home/guillaume/mon "dossier" tordu
        -> "/home/guillaume/mon\ \"dossier\"\ tordu
        """
        path = unicode(self)
        for c in list(self.special_chars):
            path = path.replace(c, u'\%s' % c)
        return u'"%s"' % path

    def normcase(self):
        return self.__class__(os.path.normcase(self))

    def normpath(self):
        # see: http://bugs.python.org/issue3426
        path = os.path.normpath(self.encode(ENCODING)).decode(ENCODING)
        return self.__class__(path)

    def realpath(self):
        # see: http://bugs.python.org/issue3426
        path = os.path.realpath(self)
        if isinstance(path, str):
            path = path.decode(ENCODING)
        return self.__class__(path)

    def splitext(self):
        return os.path.splitext(self)

    def replaceext(self, ext, old_ext=None):
        u"""
        Changes current extension to ext.
        If extension contains more than one dot (ex: .tar.gz) you can specify
        it with "old_ext" argument.

        >>> from pyLot.core import Path
        >>> p = Path(u'path.py')
        >>> p.replaceext(u'.rst')
        Path(u'path.rst')

        >>> p = Path(u'archive.tar.gz')
        >>> p.replaceext(u'.tgz')
        Path(u'archive.tar.tgz')
        >>> p.replaceext(u'.tgz', u'.tar.gz')
        Path(u'archive.tgz')
        """
        if old_ext is None:
            path, old_ext = self.splitext()
            return self.__class__(path + ext)
        else:
            path_ext = self[-len(old_ext):]
            if path_ext != old_ext:
                raise ValueError, 'old_ext %(OLD_EXT)r and path ext %(PATH_EXT)r do not match.' % dict(
                    OLD_EXT=old_ext, PATH_EXT=path_ext[1:])
            else:
                return self.__class__(self[:-len(old_ext)] + ext)

    def expanduser(self):
        return self.__class__(os.path.expanduser(self))

    def expandvars(self):
        return self.__class__(os.path.expandvars(self))

    def expand(self):
        u""" Cleans up a filename by calling expandvars(),
        expanduser(), and normpath() on it.

        This is commonly everything needed to clean up a filename
        read from a configuration file, for example.
        """
        return self.expandvars().expanduser().normpath()

    def _get_namebase(self):
        base, ext = os.path.splitext(self.name)
        return base

    def _get_ext(self):
        f, ext = os.path.splitext(unicode(self))
        return ext

    def _get_drive(self):
        drive, r = os.path.splitdrive(self)
        return self.__class__(drive)

    def _get_dirname(self):
        return self.__class__(os.path.dirname(self))

    parent = property(
        _get_dirname, None, None,
        u""" This path's parent directory, as a new path object.

        For example, Path('/usr/local/lib/libpython.so').parent == Path('/usr/local/lib')
        """)
    dirname = parent

    name = property(
        os.path.basename, None, None,
        u""" The name of this file or directory without the full path.

        For example, Path('/usr/local/lib/libpython.so').name == 'libpython.so'
        """)

    namebase = property(
        _get_namebase, None, None,
        u""" The same as path.name, but with one file extension stripped off.

        For example, Path('/home/guido/python.tar.gz').name     == 'python.tar.gz',
        but          Path('/home/guido/python.tar.gz').namebase == 'python.tar'
        """)

    ext = property(
        _get_ext, None, None,
        u""" The file extension, for example '.py'. """)

    drive = property(
        _get_drive, None, None,
        u""" The drive specifier, for example 'C:'.
        This is always empty on systems that don't use drive specifiers.
        """)

    def splitpath(self):
        u""" p.splitpath() -> Return (p.parent, p.name). """
        parent, child = os.path.split(self)
        if parent == u'':
            parent = u'.'
        return self.__class__(parent), child

    def stripext(self):
        u""" p.stripext() -> Remove one file extension from the path.

        For example, Path('/home/guido/python.tar.gz').stripext()
        returns Path('/home/guido/python.tar').
        """
        return Path(os.path.splitext(self)[0])

    if hasattr(os.path, u'splitunc'):
        def splitunc(self):
            unc, rest = os.path.splitunc(self) #@UndefinedVariable
            return self.__class__(unc), rest

        def _get_uncshare(self):
            unc, r = os.path.splitunc(self) #@UndefinedVariable
            return self.__class__(unc)

        uncshare = property(
            _get_uncshare, None, None,
            u""" The UNC mount point for this path.
            This is empty for paths on local drives. """)

    def splitall(self):
        u""" Returns a list of the path components in this path.

        The first item in the list will be a path.  Its value will be
        either os.curdir, os.pardir, empty, or the root directory of
        this path (for example, '/' or 'C:\\').  The other items in
        the list will be strings.

        path.path(`*result`) will yield the original path.
        """
        parts = []
        loc = self
        while loc != os.curdir and loc != os.pardir:
            prev = loc
            loc, child = prev.splitpath()
            loc = self.__class__(loc)
            if loc == prev:
                break
            parts.append(child)
        parts.append(loc)
        parts.reverse()
        return parts

    def shortestpath(self):
        u"""
        Returns the shortest path between abspath and relpath.
        This form is useful for display.
        """
        relpath = len(unicode(self.relpath()))
        abspath = len(unicode(self.abspath()))
        if abspath <= relpath:
            return self.abspath()
        else:
            return self.relpath()

    def relpath(self):
        u""" Returns this path as a relative path,
        based from the current working directory.
        """
        return self.__class__.cwd().relpathto(self)

    def relpathto(self, dest):
        u""" Returns a relative path from self to dest.

        If there is no relative path from self to dest, for example if
        they reside on different drives in Windows, then this returns
        dest.abspath().
        """
        if not isinstance(dest, unicode):
            raise TypeError, tr_(u'E006 - Path, unicode expected, got %(TYPE)s' % dict(
                TYPE=type(dest).__name__))

        origin = self.abspath()
        dest = self.__class__(dest).abspath()

        orig_list = origin.normcase().splitall()
        # Don't normcase dest!  We want to preserve the case.
        dest_list = dest.splitall()

        if orig_list[0] != os.path.normcase(dest_list[0]):
            # Can't get here from there.
            return dest

        # Find the location where the two paths start to differ.
        i = 0
        for start_seg, dest_seg in zip(orig_list, dest_list):
            if start_seg != os.path.normcase(dest_seg):
                break
            i += 1

        # Now i is the point where the two paths diverge.
        # Need a certain number of "os.pardir"s to work up
        # from the origin to the point of divergence.
        segments = [os.pardir.decode(ENCODING)] * (len(orig_list) - i)
        # Need to add the diverging part of dest_list.
        segments += dest_list[i:]
        if len(segments) == 0:
            # If they happen to be identical, use os.curdir.
            return self.__class__(os.curdir.decode(ENCODING))
        else:
            return self.__class__(os.path.join(*segments))


    # --- Listing, searching, walking, and matching

    def listdir(self, pattern=None):
        u""" D.listdir() -> Lists of items in this directory.

        Use D.files() or D.dirs() instead if you want a listing
        of just files or just subdirectories.

        The elements of the list are path objects.

        With the optional 'pattern' argument, this only lists
        items whose names match the given pattern.
        """
        names = os.listdir(unicode(self))
        if pattern is not None:
            names = fnmatch.filter(names, pattern)
        return [Path(unicode(self), child) for child in names]

    def dirs(self, pattern=None):
        u"""
        D.dirs() -> List of this directory's subdirectories.

        The elements of the list are path objects.
        This does not walk recursively into subdirectories
        (but see path.walkdirs).

        With the optional 'pattern' argument, this only lists
        directories whose names match the given pattern.  For
        example, d.dirs('`build-*`').
        """
        return [p for p in self.listdir(pattern) if p.isdir()]

    def files(self, pattern=None):
        u"""
        D.files() -> List of the files in this directory.

        The elements of the list are path objects.
        This does not walk into subdirectories (see path.walkfiles).

        With the optional 'pattern' argument, this only lists files
        whose names match the given pattern.  For example,
        d.files('`*.pyc`').
        """

        return [p for p in self.listdir(pattern) if p.isfile()]

    def walk(self, pattern=None):
        u"""
        D.walk() -> iterator over files and subdirs, recursively.

        The iterator yields path objects naming each child item of
        this directory and its descendants.  This requires that
        D.isdir().

        This performs a depth-first traversal of the directory tree.
        Each directory is returned just before all its children.
        """
        for child in self.listdir():
            if pattern is None or child.match(pattern):
                yield child
            if child.isdir():
                for item in child.walk(pattern):
                    yield item

    def walkdirs(self, pattern=None):
        u"""
        D.walkdirs() -> iterator over subdirs, recursively.

        With the optional 'pattern' argument, this yields only
        directories whose names match the given pattern.  For
        example, mydir.walkdirs('`*test`') yields only directories
        with names ending in 'test'.
        """
        for child in self.dirs():
            if pattern is None or child.match(pattern):
                yield child
            for subsubdir in child.walkdirs(pattern):
                yield subsubdir

    def walkfiles(self, pattern=None):
        u"""
        D.walkfiles() -> iterator over files in D, recursively.

        The optional argument, pattern, limits the results to files
        with names that match the pattern.  For example,
        mydir.walkfiles('`*.tmp`') yields only files with the .tmp
        extension.
        """
        for child in self.listdir():
            if child.isfile():
                if pattern is None or child.match(pattern):
                    yield child
            elif child.isdir():
                for f in child.walkfiles(pattern):
                    yield f

    def walkfiles_re(self, pattern=None):
        u"""
        D.walkfiles_re() -> iterator over files in D, recursively.

        The optional argument, (regex) pattern, limits the results to files
        with names that match the pattern.  For example,
        mydir.walkfiles('`.*\\.tmp`') yields only files with the .tmp
        extension.
        """
        for child in self.listdir():
            if child.isfile():
                if pattern is None or re.match(pattern, child):
                    yield child
            elif child.isdir():
                for f in child.walkfiles_re(pattern):
                    yield f

    def walkfiles_date(self, datemin, datemax=None):
        u"""
        D.walkfiles() -> iterator over files in D, recursively.
        .. warning::

         age parameter and datemin/datemax are exclusive

        """
        if not datemax:
            datemax = datetime.datetime.today()

        for child in self.listdir():
            if child.isfile():
                if datemin <= child.mtime_datetime() <= datemax:
                    yield child
            elif child.isdir() and not child.islink():
                for f in child.walkfiles_date(datemin, datemax):
                    yield f

    def walkfiles_age(self, days, hours, minutes, seconds):
        u"""
        D.walkfiles() -> iterator over files in D, recursively.
        .. warning::

         age parameter and datemin/datemax are exclusive

        """
        raise NotImplementedError

    #        if not datemax :
    #          datemax = datetime.datetime.today()
    #
    #        for child in self.listdir():
    #            if child.isfile():
    #                if datemin <= child.mtime_datetime() <= datemax :
    #                    yield child
    #            elif child.isdir():
    #                for f in child.walkfiles_mtime(datemin, datemax):
    #                    yield f

    def match(self, pattern):
        u"""
        Return True if self.name matches the given pattern.

        pattern - A filename pattern with wildcards,
        for example `*.py`.
        """
        return fnmatch.fnmatch(self.name, pattern)

    def matchcase(self, pattern):
        u"""
        Test whether the path matches pattern, returning true or
        false; the comparison is always case-sensitive.
        """
        return fnmatch.fnmatchcase(self.name, pattern)

    def glob(self, pattern):
        u"""
        Return a list of path objects that match the pattern.

        pattern - a path relative to this directory, with wildcards.

        For example, Path('/users').glob('`*/bin/*`') returns a list
        of all the files users have in their bin directories.
        """
        return map(Path, glob.glob(unicode(Path(self, pattern))))

    # --- Methods for querying the filesystem.

    exists = os.path.exists
    isabs = os.path.isabs
    isdir = os.path.isdir
    isfile = os.path.isfile
    islink = os.path.islink
    ismount = os.path.ismount

    if hasattr(os.path, u'samefile'):
        samefile = os.path.samefile

    #    def atime_local(self):
    #      return time.localtime(self.atime())
    #
    #    def mtime_local(self):
    #      return time.localtime(self.mtime())
    #
    #    def ctime_local(self):
    #      return time.localtime(self.ctime())

    def atime_datetime(self):
        return datetime.datetime.fromtimestamp(self.atime())

    def mtime_datetime(self):
        return datetime.datetime.fromtimestamp(self.mtime())

    def ctime_datetime(self):
        return datetime.datetime.fromtimestamp(self.ctime())

    def atime(self):
        u"""Last access time of the file."""
        return os.path.getatime(self)

    def mtime(self):
        u"""Last-modified time of the file."""
        return os.path.getmtime(self)

    def ctime(self):
        u"""
        Return the system's ctime which, on some systems (like Unix)
        is the time of the last change, and, on others (like Windows),
        is the creation time for path.

        The return value is a number giving the number of seconds
        since the epoch (see the time module). Raise os.error if the
        file does not exist or is inaccessible.
        """
        return os.path.getctime(self)

    def size(self):
        u"""Size of the file, in bytes."""
        return os.path.getsize(self)

    if hasattr(os, u'access'):
        def access(self, mode):
            u""" Return true if current user has access to this path.

            mode - One of the constants os.F_OK, os.R_OK, os.W_OK, os.X_OK
            """
            return os.access(self, mode)

    def stat(self):
        u""" Perform a stat() system call on this path. """
        return os.stat(self)

    def lstat(self):
        u""" Like path.stat(), but do not follow symbolic links. """
        return os.lstat(self)

    if hasattr(os, u'statvfs'):
        def statvfs(self):
            u""" Perform a statvfs() system call on this path. """
            return os.statvfs(self)

    if hasattr(os, u'pathconf'):
        def pathconf(self, name):
            return os.pathconf(self, name)


    # --- Modifying operations on files and directories

    def utime(self, times):
        u""" Set the access and modified times of this file. """
        os.utime(self, times)

    def chmod(self, mode):
        os.chmod(self, mode)

    if hasattr(os, u'chown'):
        def chown(self, uid, gid):
            os.chown(self, uid, gid)

    def rename(self, new):
        os.rename(self, new)

    def renames(self, new):
        os.renames(self, new)


    # --- Create/delete operations on directories

    def mkdir(self, mode=0777):
        os.mkdir(self, mode)

    def makedirs(self, mode=0777):
        os.makedirs(self, mode)

    def rmdir(self):
        os.rmdir(self)

    def removedirs(self):
        os.removedirs(self)


    # --- Modifying operations on files

    def touch(self):
        u""" Set the access/modified times of this file to the current time.
        Create the file if it does not exist.
        """
        fd = os.open(self, os.O_WRONLY | os.O_CREAT, 0666)
        os.close(fd)
        os.utime(self, None)

    def remove(self):
        os.remove(self)

    def unlink(self):
        os.unlink(self)


    # --- Links

    if hasattr(os, u'link'):
        def link(self, newpath):
            u""" Create a hard link at 'newpath', pointing to this file. """
            os.link(self, newpath)

    if hasattr(os, u'symlink'):
        def symlink(self, newlink):
            u""" Create a symbolic link at 'newlink', pointing here. """
            os.symlink(self, newlink)

    if hasattr(os, u'readlink'):
        def readlink(self):
            u""" Return the path to which this symbolic link points.

            The result may be an absolute or a relative path.
            """
            return self.__class__(os.readlink(self))

        def readlinkabs(self):
            u""" Return the path to which this symbolic link points.

            The result is always an absolute path.
            """
            p = self.readlink()
            if p.isabs():
                return p
            else:
                return self.__class__(self.parent, p).abspath()

    # --- High-level functions from shutil

    copyfile = shutil.copyfile
    copymode = shutil.copymode
    copystat = shutil.copystat
    copy = shutil.copy
    copy2 = shutil.copy2
    copytree = shutil.copytree
    if hasattr(shutil, u'move'):
        move = shutil.move
    rmtree = shutil.rmtree

    # --- Special stuff from os

    if hasattr(os, u'chroot'):
        def chroot(self):
            os.chroot(self)

    # --- Reading or writing an entire file at once.

    def open(self, mode='r'):
        """ Open this file.  Return a file object. """
        return open(self, mode)

    def bytes(self):
        """ Open this file, read all bytes, return them as a string. """
        f = self.open('rb')
        try:
            return f.read()
        finally:
            f.close()

    def write_bytes(self, bytes, append=False):
        """ Open this file and write the given bytes to it.

        Default behavior is to overwrite any existing file.
        Call p.write_bytes(bytes, append=True) to append instead.
        """
        if append:
            mode = 'ab'
        else:
            mode = 'wb'
        f = self.open(mode)
        try:
            f.write(bytes)
        finally:
            f.close()

    def text(self, encoding=None, errors='strict'):
        r""" Open this file, read it in, return the content as a string.

        This uses 'U' mode in Python 2.3 and later, so '\r\n' and '\r'
        are automatically translated to '\n'.

        Optional arguments:

        encoding - The Unicode encoding (or character set) of
            the file.  If present, the content of the file is
            decoded and returned as a unicode object; otherwise
            it is returned as an 8-bit str.
        errors - How to handle Unicode errors; see help(str.decode)
            for the options.  Default is 'strict'.
        """
        if encoding is None:
            # 8-bit
            f = self.open('U')
            try:
                return f.read()
            finally:
                f.close()
        else:
            # Unicode
            f = codecs.open(self, 'r', encoding, errors)
            # (Note - Can't use 'U' mode here, since codecs.open
            # doesn't support 'U' mode, even in Python 2.3.)
            try:
                t = f.read()
            finally:
                f.close()
            return (t.replace(u'\r\n', u'\n')
            .replace(u'\r\x85', u'\n')
            .replace(u'\r', u'\n')
            .replace(u'\x85', u'\n')
            .replace(u'\u2028', u'\n'))

    def write_text(self, text, encoding=None, errors='strict', linesep=os.linesep, append=False):
        r""" Write the given text to this file.

        The default behavior is to overwrite any existing file;
        to append instead, use the 'append=True' keyword argument.

        There are two differences between path.write_text() and
        path.write_bytes(): newline handling and Unicode handling.
        See below.

        Parameters:

          - text - str/unicode - The text to be written.

          - encoding - str - The Unicode encoding that will be used.
            This is ignored if 'text' isn't a Unicode string.

          - errors - str - How to handle Unicode encoding errors.
            Default is 'strict'.  See help(unicode.encode) for the
            options.  This is ignored if 'text' isn't a Unicode
            string.

          - linesep - keyword argument - str/unicode - The sequence of
            characters to be used to mark end-of-line.  The default is
            os.linesep.  You can also specify None; this means to
            leave all newlines as they are in 'text'.

          - append - keyword argument - bool - Specifies what to do if
            the file already exists (True: append to the end of it;
            False: overwrite it.)  The default is False.


        --- Newline handling.

        write_text() converts all standard end-of-line sequences
        ('\n', '\r', and '\r\n') to your platform's default end-of-line
        sequence (see os.linesep; on Windows, for example, the
        end-of-line marker is '\r\n').

        If you don't like your platform's default, you can override it
        using the 'linesep=' keyword argument.  If you specifically want
        write_text() to preserve the newlines as-is, use 'linesep=None'.

        This applies to Unicode text the same as to 8-bit text, except
        there are three additional standard Unicode end-of-line sequences:
        u'\x85', u'\r\x85', and u'\u2028'.

        (This is slightly different from when you open a file for
        writing with fopen(filename, "w") in C or file(filename, 'w')
        in Python.)


        --- Unicode

        If 'text' isn't Unicode, then apart from newline handling, the
        bytes are written verbatim to the file.  The 'encoding' and
        'errors' arguments are not used and must be omitted.

        If 'text' is Unicode, it is first converted to bytes using the
        specified 'encoding' (or the default encoding if 'encoding'
        isn't specified).  The 'errors' argument applies only to this
        conversion.

        """
        if isinstance(text, unicode):
            if linesep is not None:
                # Convert all standard end-of-line sequences to
                # ordinary newline characters.
                text = (text.replace(u'\r\n', u'\n')
                .replace(u'\r\x85', u'\n')
                .replace(u'\r', u'\n')
                .replace(u'\x85', u'\n')
                .replace(u'\u2028', u'\n'))
                text = text.replace(u'\n', linesep)
            if encoding is None:
                encoding = sys.getdefaultencoding()
            bytes = text.encode(encoding, errors)
        else:
            # It is an error to specify an encoding if 'text' is
            # an 8-bit string.
            assert encoding is None

            if linesep is not None:
                text = (text.replace('\r\n', '\n')
                .replace('\r', '\n'))
                bytes = text.replace('\n', linesep)

        self.write_bytes(bytes, append)

    def lines(self, encoding=None, errors='strict', retain=True):
        r""" Open this file, read all lines, return them in a list.

        Optional arguments:
            encoding - The Unicode encoding (or character set) of
                the file.  The default is None, meaning the content
                of the file is read as 8-bit characters and returned
                as a list of (non-Unicode) str objects.
            errors - How to handle Unicode errors; see help(str.decode)
                for the options.  Default is 'strict'
            retain - If true, retain newline characters; but all newline
                character combinations ('\r', '\n', '\r\n') are
                translated to '\n'.  If false, newline characters are
                stripped off.  Default is True.

        This uses 'U' mode in Python 2.3 and later.
        """
        if encoding is None and retain:
            f = self.open('U')
            try:
                return f.readlines()
            finally:
                f.close()
        else:
            return self.text(encoding, errors).splitlines(retain)

    def write_lines(self, lines, encoding=None, errors='strict',
        linesep=os.linesep, append=False):
        r""" Write the given lines of text to this file.

        By default this overwrites any existing file at this path.

        This puts a platform-specific newline sequence on every line.
        See 'linesep' below.

        lines - A list of strings.

        encoding - A Unicode encoding to use.  This applies only if
            'lines' contains any Unicode strings.

        errors - How to handle errors in Unicode encoding.  This
            also applies only to Unicode strings.

        linesep - The desired line-ending.  This line-ending is
            applied to every line.  If a line already has any
            standard line ending ('\r', '\n', '\r\n', u'\x85',
            u'\r\x85', u'\u2028'), that will be stripped off and
            this will be used instead.  The default is os.linesep,
            which is platform-dependent ('\r\n' on Windows, '\n' on
            Unix, etc.)  Specify None to write the lines as-is,
            like file.writelines().

        Use the keyword argument append=True to append lines to the
        file.  The default is to overwrite the file.  Warning:
        When you use this with Unicode data, if the encoding of the
        existing data in the file is different from the encoding
        you specify with the encoding= parameter, the result is
        mixed-encoding data, which can really confuse someone trying
        to read the file later.
        """
        if append:
            mode = 'ab'
        else:
            mode = 'wb'
        f = self.open(mode)
        try:
            for line in lines:
                isUnicode = isinstance(line, unicode)
                if linesep is not None:
                    # Strip off any existing line-end and add the
                    # specified linesep string.
                    if isUnicode:
                        if line[-2:] in (u'\r\n', u'\x0d\x85'):
                            line = line[:-2]
                        elif line[-1:] in (u'\r', u'\n',
                        u'\x85', u'\u2028'):
                            line = line[:-1]
                    else:
                        if line[-2:] == '\r\n':
                            line = line[:-2]
                        elif line[-1:] in ('\r', '\n'):
                            line = line[:-1]
                    line += linesep
                if isUnicode:
                    if encoding is None:
                        encoding = sys.getdefaultencoding()
                    line = line.encode(encoding, errors)
                f.write(line)
        finally:
            f.close()

    def read_md5(self):
        """ Calculate the md5 hash for this file.

        This reads through the entire file.
        """
        f = self.open('rb')
        try:
            m = md5()
            while True:
                d = f.read(8192)
                if not d:
                    break
                m.update(d)
        finally:
            f.close()
        return m.digest()

    def info_written(self, stream=sys.stdout):
        print >> stream, '%s written' % self.shellpath()

    def info_deleted(self, stream=sys.stdout):
        print >> stream, '%s deleted' % self.shellpath()

    def info_created(self, stream=sys.stdout):
        print >> stream, '%s created' % self.shellpath()

class FilePath(Path):
    u"""
    Restricted Path for files only
    """

class DirPath(Path):
    u"""
    Restricted Path for directories only
    """

class UserConfigDir(Path):
    u"""
    current directory is always ${HOME}
    """

    def __new__(cls, *args):
        u"""
        Creates a new path object concatenating the *args.  *args
        may only contain Path objects or strings.  If *args is
        empty, an exception is raised.
        """
        home = Path(u'~').expanduser()
        path = Path(*args)
        for code in (u'~', u'$HOME', u'$USER'):
            if code in path:
                path = path.expanduser().expandvars()
        path = Path(home, path).abspath()
        dirname = home.relpathto(path)

        if path == home:
            raise ValueError, u'$HOME is not allowed'
        elif path.isabs() and not path.startswith(home):
            raise ValueError, u'Absolute path (%s) is not allowed, path must be relative to %s' % (path, home)
        #      elif path.isfile() :
        #        raise ValueError, u'path (%s) exists and is a file' % path
        #      elif not dirname.startswith(u'.') :
        #        raise ValueError, u'path must start with "."'
        else:
            return Path.__new__(cls, path)


    def rmtree(self, ignore_errors=False, onerror=None):
        pass

    def check(self):
        u"""
        Create directory if necessary, check write permission and return
        True if all is OK, else False
        """
        all_ok = True
        if not self.exists():
            try:
                self.makedirs()
            except OSError:
                uprint(u'cannot make directory %s' % self, file=sys.stderr)
                all_ok = False
        else:
            fpath = Path(self, u'tmptestfile')
            try:
                f = open(fpath, u'w')
            except IOError:
                uprint(u'Permission denied: %s' % self, file=sys.stderr)
                all_ok = False
            else:
                f.close()
                fpath.remove()

        return all_ok

def format_path(path, abspath=True, expanduser=True, pathbook=None, pathid=None):
    if path is None or path is u'':
        if pathid is None or pathbook is None:
            return None
        else:
            path = pathbook(pathid)
    path = Path(path)
    if expanduser: path = path.expanduser()
    if abspath: path = path.abspath()
    return path

if __name__ == '__main__':
    for path in [
        UserConfigDir(u'.dhtoolkit'),
        #    UserConfigDir(u'/test'),
        UserConfigDir(u'/home/baty/.dhtoolkit'),
        UserConfigDir(u'~', u'.dhtoolkit'),
        UserConfigDir(u'$HOME', u'.dhtoolkit'),
        #    UserConfigDir(u''),
        #    UserConfigDir(u'/home/baty/test', u'..'),
        #    UserConfigDir(u'$HOME', u'.'),
        #    UserConfigDir(u'/home/autre/.dhtoolkit'),
        #    UserConfigDir(u'/home/baty'),
    ]:
        print type(path), path, path.isabs()
